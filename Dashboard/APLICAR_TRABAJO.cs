//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dashboard
{
    using System;
    using System.Collections.Generic;
    
    public partial class APLICAR_TRABAJO
    {
        public int ID { get; set; }
        public int PUBLICACION_ID { get; set; }
        public int PERSONA_ID { get; set; }
        public decimal PRESUPUESTO { get; set; }
        public System.DateTime FECHA { get; set; }
    
        public virtual PERSONA PERSONA { get; set; }
        public virtual PUBLICACION_TRABAJO PUBLICACION_TRABAJO { get; set; }
    }
}
