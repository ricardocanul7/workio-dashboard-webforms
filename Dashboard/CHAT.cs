//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dashboard
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHAT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CHAT()
        {
            this.MENSAJEs = new HashSet<MENSAJE>();
        }
    
        public int ID { get; set; }
        public int PERSONA1_ID { get; set; }
        public int PERSONA2_ID { get; set; }
    
        public virtual PERSONA PERSONA { get; set; }
        public virtual PERSONA PERSONA1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MENSAJE> MENSAJEs { get; set; }
    }
}
