//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dashboard
{
    using System;
    using System.Collections.Generic;
    
    public partial class ELEMENTO_PERMITIDO
    {
        public int ID { get; set; }
        public int PERFIL_ID { get; set; }
        public int ELEMENTOS_PERMISOS_ID { get; set; }
    
        public virtual ELEMENTOS_PERMISOS ELEMENTOS_PERMISOS { get; set; }
        public virtual PERFIL PERFIL { get; set; }
    }
}
