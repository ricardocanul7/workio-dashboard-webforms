﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard.MasterPage
{
    public partial class Dashboard : System.Web.UI.MasterPage
    {
        private USUARIO userLogged = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["loggedUser"] != null)
            {
                this.userLogged = (USUARIO)Session["loggedUser"];
                lbl_username.Text = userLogged.PERSONA.NOMBRES + " " + userLogged.PERSONA.APELLIDO_P;
            }
            else
            {
                Response.Redirect("~/UI/Login.aspx");
            }
        }
    }
}