﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Base.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Dashboard.UI.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head_base" runat="server">
    <title>Login</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_contenido_base" runat="server">
    <div class="container">
    <div class="row">

    </div>
    <div class="row justify-content-center">
        <div class="col">

        </div>
        <div class="col" style="margin-top: 150px;">
            <img src="https://img.icons8.com/color/48/000000/work.png" style="margin-left: auto; margin-right: auto; display: block;">
            <form class="col-md-offset-4" runat="server">
                <div class="form-group">
                    <label for="InputUsername">Username</label>
                    <input type="text" class="form-control" id="idUsername" aria-describedby="usernameHelp" runat="server">
                    <small id="Username" class="form-text text-muted">Nunca compartas tus credenciales</small>
                </div>
                <div class="form-group">
                    <label for="InputPassword">Contraseña</label>
                    <input type="password" class="form-control" id="idPassword" runat="server">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="passCheck">
                    <label class="form-check-label" for="passCheck">Recordar contraseña</label>
                </div>
                <asp:Button Text="Iniciar sesión asp" type="submit" class="btn btn-primary" style="margin-left: auto; margin-right: auto; display: block;" runat="server" OnClick="IniciarSesion"/>
            </form>
        </div>
        <div class="col">
        </div>
    </div>
</div>


</asp:Content>
