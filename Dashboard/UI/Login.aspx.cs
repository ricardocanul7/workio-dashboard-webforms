﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard.UI
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void IniciarSesion(object sender, EventArgs e)
        {
            string username = idUsername.Value;
            string password = idPassword.Value;

            USUARIO usuario = new USUARIO();

            var dbEntities = new EntitiesWorkioDB();

            var query = dbEntities.USUARIOs.Where(u => u.USERNAME == username && u.CONTRASENIA == password);

            if (query.Any())
            {
                usuario = query.FirstOrDefault() as USUARIO;

                Session["loggedUser"] = usuario;

                Response.Redirect("~/UI/Inicio.aspx");
            }
            else
            {
                Response.Write("<script>alert('Usuario o contraseña incorrecta')</script>");
            }
        }
    }
}